Enjoy the ultimate in apartment living at Havenwood Townhomes! Experience the luxury of large living spaces and newly remodeled finishes throughout. Choose two or three bedroom townhome layouts to best fit your lifestyle.

Address: 4312 Westport Rd, Columbus, OH 43228, USA

Phone: 614-504-4490
